# Struct Theme
A theme to match [destruct.dev](https://destruct.dev)

<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/struct%20-%20zed-theme.webp" alt="struct-zed-theme" width="100%">
